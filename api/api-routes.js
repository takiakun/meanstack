// Filename: api-routes.js
// Initialize express router
let router = require("express").Router();
// Set default API response
router.get("/", function(req, res) {
  res.json({
    status: "API Its Working",
    message: "Welcome to RESTHub crafted with love!"
  });
});

// Import user controller
var userController = require("./controllers/users.controller");
// user routes
router
  .route("/users")
  .get(userController.index)
  .post(userController.new);
router
  .route("/user/:user_id")
  .get(userController.view)
  .patch(userController.update)
  .put(userController.update)
  .delete(userController.delete);
router.route("/user/authenticate").post(userController.authenticate);
router
  .route("/user/changepassword/:user_id")
  .put(userController.changePassword);

// Import Contact controller
var contactController = require("./controllers/contact.controller");
// Contact routes

router
  .route("/public/contacts")
  .post(contactController.new);
router
  .route("/contacts")
  .get(contactController.index)
  .post(contactController.new);
router
  .route("/contact/:contact_id")
  .get(contactController.view)
  .patch(contactController.update)
  .put(contactController.update)
  .delete(contactController.delete);
  var tsInfoController = require("./controllers/TSInfo.controller");
router
  .route("/public/tsInfo")
  .get(tsInfoController.index);
  
router
  .route("/public/tsInfo/:_id")
  .get(tsInfoController.view);
  
  router
  .route("/tsInfo")
  .get(tsInfoController.index)
  .post(tsInfoController.new);
router
  .route("/tsInfo/:_id")
  .get(tsInfoController.view)
  .patch(tsInfoController.update)
  .put(tsInfoController.update)
  .delete(tsInfoController.delete);

// Export API routes
module.exports = router;
