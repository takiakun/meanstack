// tsInfoController.js
// Import tsInfo model
TSInfo = require("../models/TSInfo.model");

exports.index = async function (req, res) {
  TSInfo.find({},'info icon',function (err, tsInfos) {
    if (err) {
      res.json({
        status: "error",
        message: err
      });
    }
    console.log(JSON.stringify(tsInfos));
    res.json({
      status: "success",
      message: "TSInfos retrieved successfully",
      data: tsInfos
    });
  });
};

// Handle create user actions
exports.new = function (req, res) {
  TSInfo.find({ info: req.body.info.trim() }, function (err, tsInfos) {
    if (err) {
      res.json({
        status: "error",
        message: err
      });
    }
    if (tsInfos && tsInfos.length > 0) {
      res.status(400).send({
        status: "error",
        message: req.body.info + " is already exist"
      });
    } else {
      var tsInfo = new TSInfo();
      var tsInfoObj = req.body;
      Object.keys(tsInfoObj).forEach((key, index) => {
        tsInfo[ key ] = tsInfoObj[ key ];
      });

      // save the tsInfo and check for errors
      tsInfo.save(function (err) {
        if (err) {
          res.status(400).json({
            status: "error",
            error: err
          });
        }
        res.json({
          message: "New tsInfo created!",
          data: tsInfo
        });
      });
    }
  });
};
  // Handle view tsInfo info
  exports.view = function (req, res) {
    TSInfo.findById(req.params._id, function (err, tsInfo) {
      if (err) {
        res.status(400).json({
          status: "error",
          error: err
        });
      }
      res.json({
        message: "TSInfo details loading..",
        data: tsInfo
      });
    });
  };
  // Handle update tsInfo info
  exports.update = function (req, res) {
    TSInfo.findByIdAndUpdate(
      req.params._id,
      req.body,
      { new: true },
      function (err, tsInfo) {
        if (err) {
          res.status(400).json({
            status: "error",
            error: err
          });
        }

        // save the tsInfo and check for errors
        tsInfo.save(function (err) {
          if (err) res.json(err);
          res.json({
            message: "TSInfo Info updated",
            data: tsInfo
          });
        });
      }
    );
  };
  // Handle delete state
  exports.delete = function (req, res) {
    TSInfo.remove(
      {
        _id: req.params._id
      },
      function (err, state) {
        if (err) {
          res.status(400).json({
            status: "error",
            error: err
          });
        }
        res.json({
          status: "success",
          message: "TSInfo deleted"
        });
      }
    );
  };
