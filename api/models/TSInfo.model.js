// contact.model.js
var mongoose = require("mongoose");
// Setup schema
var tsInfoSchema = mongoose.Schema({
  info: {
    type: String,
    required: true
  },
  details: {
    type: String,
    required: true
  }
});
// Export Contact model
module.exports = mongoose.model("tsinfos", tsInfoSchema);
