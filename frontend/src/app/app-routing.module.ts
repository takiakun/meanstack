import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./core/guards";
import { LoginComponent } from "./feature/user/login/login.component";
import { RegisterComponent } from "./feature/user/register/register.component";
const routes: Routes = [
  {
    path: "",
    loadChildren: () => import("./feature/landing/landing.module").then((module) => module.LandingModule)
  },
  {
    path: "admin",
    loadChildren: () => import("./feature/user/user.module").then((module) => module.UserModule)
  },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: "enabled",
      relativeLinkResolution: "legacy"
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
