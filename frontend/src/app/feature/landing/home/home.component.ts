import { Component, OnInit, TemplateRef } from '@angular/core';
import { CarouselConfig } from 'ngx-bootstrap/carousel';
import { HomeLandingService } from './home.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ValidationService } from "../../../core/components";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: 'app-landing-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [
    { provide: CarouselConfig, useValue: { interval: 1500, noPause: true, showIndicators: true } },
    HomeLandingService
  ]
})
export class LandingHomeComponent implements OnInit {
  daotaoList = [];
  modalRef: BsModalRef;
  selectedModel = -1;
  contactForm: FormGroup;
  constructor(public homeLandingService: HomeLandingService, private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private validationService: ValidationService,
    private toastrService: ToastrService) {
    
  }

  ngOnInit(): void {
    this.getListDaoTao();
    this.createForm();
  }

  createForm(): void {
    this.contactForm = this.formBuilder.group({
      firstName: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(35)]],
      lastName: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(35)]],
      email: ["", [Validators.required]],
      mobile: ["", [Validators.required]],
      messages: ["", [Validators.required]],
    });
  }

  send(): void {
    console.log(this.contactForm);
    if (this.contactForm.valid) {
      this.homeLandingService.create(this.contactForm.value).subscribe(
        (data) => {
          this.toastrService.success("Send Request Success, Administrator will contact you soon", "Success");
          this.contactForm.reset();
        },
  
        (error) => {}
      );
    }
    else {
      this.toastrService.error("Please full fill info or Your Phone Number is have in my system, please using another phone number or try again!", "Error");
    }

  }
  getListDaoTao() {
    this.homeLandingService.getAll().subscribe(res => {
      this.daotaoList = res;
      console.log(res);
    })
  }
  openModal(template: TemplateRef<any>,id) {
    this.homeLandingService.getById(id).subscribe(res => {
      this.selectedModel = res;
      this.modalRef = this.modalService.show(template);
      console.log(res);
    })

  }

}
