import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "../../../../environments/environment";

@Injectable()
export class HomeLandingService {
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(environment.apiEndpoint + "/public/tsInfo").pipe(
      map((res: any) => {
        return res.data;
      })
    );
  }

  getById(_id: string) {
    return this.http.get(environment.apiEndpoint + "/public/tsInfo/" + _id).pipe(
      map((res: any) => {
        return res.data;
      })
    );
  }
  create(contact: any) {
    return this.http.post(environment.apiEndpoint + "/public/contacts", contact).pipe(
      map((res: any) => res.data)
    );
  }
}
