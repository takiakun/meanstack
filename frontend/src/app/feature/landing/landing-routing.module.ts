import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LandingHomeComponent } from "./home/home.component";

const userRoutes: Routes = [
  {
    path: "",
    component: LandingHomeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class LandingRoutingModule {}
