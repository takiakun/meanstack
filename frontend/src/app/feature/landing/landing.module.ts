import { NgModule } from "@angular/core";
import { LandingHomeComponent } from "./home/home.component";
import { LandingRoutingModule } from "./landing-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ReactiveFormsModule } from "@angular/forms";
@NgModule({
  declarations: [LandingHomeComponent],
  imports: [CommonModule,LandingRoutingModule, HttpClientModule,
    CarouselModule.forRoot(), ModalModule.forRoot(), ReactiveFormsModule],
  bootstrap: []
})
export class LandingModule {}
