import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TsInfoService } from "../tsInfo.service";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "app-tsInfo-details",
  templateUrl: "./tsInfo-details.component.html",
  styleUrls: ["./tsInfo-details.component.scss"]
})
export class TsInfoDetailsComponent implements OnInit {
  tsInfo: any;
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
  private tsInfoService: TsInfoService, private toastrService: ToastrService) { }

  edit(): void {
    this.router.navigate(["/admin/tsinfos/edit/" + this.tsInfo._id]);
  }
  ngOnInit(): void {
    console.log(this.activatedRoute.snapshot.data)
    this.tsInfo = this.activatedRoute.snapshot.data.tsInfoDetails;
  }
  delete(): void {
    this.tsInfoService.delete(this.tsInfo._id).subscribe(
      (data) => {
        this.toastrService.success("TsInfo deleted successfully", "Success");
        this.router.navigate(["/admin/tsinfos"]);
      },

      (error) => {}
    );
  }
}
