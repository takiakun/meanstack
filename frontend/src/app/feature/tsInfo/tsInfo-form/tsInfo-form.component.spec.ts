import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TsInfoFormComponent } from './tsInfo-form.component';

describe('TsInfoFormComponent', () => {
  let component: TsInfoFormComponent;
  let fixture: ComponentFixture<TsInfoFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TsInfoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsInfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
