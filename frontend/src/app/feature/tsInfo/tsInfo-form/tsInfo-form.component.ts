import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ValidationService } from "../../../core/components";
import { TsInfoService } from "../tsInfo.service";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { AngularEditorConfig } from '@kolkov/angular-editor';
@Component({
  selector: "app-tsInfo-form",
  templateUrl: "./tsInfo-form.component.html",
  styleUrls: ["./tsInfo-form.component.scss"]
})
  
export class TsInfoFormComponent implements OnInit {
  tsInfoForm: FormGroup;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '15rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    defaultParagraphSeparator: 'p',
    defaultFontName: 'Arial',
    toolbarHiddenButtons: [
      ['bold']
      ],
    customClasses: [
      {
        name: "quote",
        class: "quote",
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: "titleText",
        class: "titleText",
        tag: "h1",
      },
    ]
  };
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private validationService: ValidationService,
    private tsInfoService: TsInfoService,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService
  ) {}

  createForm(): void {
    this.tsInfoForm = this.formBuilder.group({
      _id: ["", []],
      info: ["", [Validators.required, Validators.minLength(2), Validators.maxLength(35)]],
      icon: [""],
      details: ["", [Validators.required]]
    });
  }

  delete(): void {
    this.tsInfoService.update(this.tsInfoForm.value._id).subscribe(
      (data) => {
        this.toastrService.success("TsInfo deleted successfully", "Success");
        this.router.navigate(["/admin/tsinfos"]);
      },

      (error) => {}
    );
  }
  submit(): void {
    const tsInfo = this.tsInfoForm.value;
    if (tsInfo._id) {
      this.update(tsInfo);
    } else {
      delete tsInfo._id;
      this.save(tsInfo);
    }
  }

  save(tsInfo: any): void {
    this.tsInfoService.create(tsInfo).subscribe(
      (data) => {
        this.toastrService.success("TsInfo created successfully", "Success");
        this.router.navigate(["/admin/tsinfos"]);
      },

      (error) => {}
    );
  }
  update(tsInfo: any): void {
    this.tsInfoService.update(tsInfo).subscribe(
      (data) => {
        this.toastrService.success("TsInfo updated successfully", "Success");
        this.router.navigate(["/admin/tsinfos"]);
      },

      (error) => {}
    );
  }
  ngOnInit(): void {
    this.createForm();
    const tsInfoDetails = this.activatedRoute.snapshot.data.tsInfoDetails;
    if (tsInfoDetails) {
      this.tsInfoForm.patchValue(tsInfoDetails);
    }
  }
}
