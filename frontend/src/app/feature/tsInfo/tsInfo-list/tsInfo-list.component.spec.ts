import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TsInfoListComponent } from './tsInfo-list.component';

describe('TsInfoListComponent', () => {
  let component: TsInfoListComponent;
  let fixture: ComponentFixture<TsInfoListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TsInfoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
