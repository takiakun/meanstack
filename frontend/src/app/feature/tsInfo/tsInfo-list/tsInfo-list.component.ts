import { Component, OnInit } from "@angular/core";
import { SortType, SelectionType, ColumnMode } from "@swimlane/ngx-datatable";
import { TsInfoService } from "../tsInfo.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-tsInfo-list",
  templateUrl: "./tsInfo-list.component.html",
  styleUrls: ["./tsInfo-list.component.scss"]
})
export class TsInfoListComponent implements OnInit {
  SortType = SortType;
  tsInfos: any;
  selected = [];
  SelectionType = SelectionType;
  ColumnMode = ColumnMode;
  columns = [
    { prop: "info", name:"Tiêu Đề"},
    { prop: "icon"},
  ];
  constructor(private tsInfoService: TsInfoService, private router: Router) {}
  getAll(): void {
    this.tsInfoService.getAll().subscribe(
      (data) => {
        console.log(data);
        this.tsInfos = data;
      },

      (error) => {}
    );
  }
  onSelect(selected: any): void {
    console.log("Select Event", selected, this.selected);
    this.router.navigate(["/admin/tsinfos/details/" + this.selected[0]._id]);
  }
  ngOnInit(): void {
    this.getAll();
  }
}
