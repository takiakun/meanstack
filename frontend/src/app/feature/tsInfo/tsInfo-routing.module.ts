import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LayoutComponent } from "../../core/layout/layout.component";
import { TsInfoListComponent } from "./tsInfo-list/tsInfo-list.component";
import { TsInfoFormComponent } from "./tsInfo-form/tsInfo-form.component";
import { TsInfoDetailsComponent } from "./tsInfo-details/tsInfo-details.component";
import { TsInfoDetailsResolver } from "./tsInfo.resolver";

const tsInfoRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        component: TsInfoListComponent
      },
      {
        path: "create",
        component: TsInfoFormComponent
      },
      {
        path: "edit/:tsInfoId",
        component: TsInfoFormComponent,
        resolve: { tsInfoDetails: TsInfoDetailsResolver }
      },
      {
        path: "details/:tsInfoId",
        component: TsInfoDetailsComponent,
        resolve: { tsInfoDetails: TsInfoDetailsResolver }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(tsInfoRoutes)],
  exports: [RouterModule]
})
export class TsInfoRoutingModule {}
