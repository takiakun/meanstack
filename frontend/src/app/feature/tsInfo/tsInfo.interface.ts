export interface TsInfo {
  _id: string;
  lastName: string;
  mobile: string;
  workPhone: string;
  email: string;
}
