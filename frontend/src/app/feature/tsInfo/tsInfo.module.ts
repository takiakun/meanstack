import { NgModule } from "@angular/core";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { TsInfoFormComponent } from "./tsInfo-form/tsInfo-form.component";
import { TsInfoListComponent } from "./tsInfo-list/tsInfo-list.component";
import { TsInfoDetailsComponent } from "./tsInfo-details/tsInfo-details.component";
import { ReactiveFormsModule } from "@angular/forms";
import { CoreModule } from "../../core/core.module";
import { SharedModule } from "../../shared/shared.module";
import { TsInfoRoutingModule } from "./tsInfo-routing.module";
import { TsInfoService } from "./tsInfo.service";
import { TsInfoDetailsResolver } from "./tsInfo.resolver";
import { AngularEditorModule } from '@kolkov/angular-editor';
@NgModule({
  declarations: [TsInfoFormComponent, TsInfoListComponent, TsInfoDetailsComponent],
  imports: [TsInfoRoutingModule, NgxDatatableModule, ReactiveFormsModule, CoreModule.forRoot(), SharedModule.forRoot(),AngularEditorModule],
  providers: [TsInfoService, TsInfoDetailsResolver],
  bootstrap: []
})
export class TsInfoModule {}
