import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { map } from "rxjs/operators";
import { TsInfoService } from "./tsInfo.service";

@Injectable()
export class TsInfoDetailsResolver implements Resolve<any> {
  constructor(private tsInfoService: TsInfoService) {}

  resolve(route: ActivatedRouteSnapshot): any {
    console.log(route.paramMap.get("tsInfoId"));
    return this.tsInfoService.getById(route.paramMap.get("tsInfoId")).pipe(
      map((result: any) => {
        return result;
      })
    );
  }
}
