import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { AuthGuard } from "../../../app/core/guards";
import { LayoutComponent } from "../../../app/core/layout/layout.component";
import { ProfileComponent } from "./profile/profile.component";

const userRoutes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        component: HomeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: "profile",
        component: ProfileComponent,
        canActivate: [AuthGuard]
      },
      {
    path: "contacts",
    canActivate: [AuthGuard],
    loadChildren: () => import("../../feature/contact/contact.module").then((module) => module.ContactModule)
      },
      {
        path: "tsinfos",
        canActivate: [AuthGuard],
        loadChildren: () => import("../../feature/tsInfo/tsInfo.module").then((module) => module.TsInfoModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
