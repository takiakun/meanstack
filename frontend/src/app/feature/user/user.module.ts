import { NgModule } from "@angular/core";
import { HomeComponent } from "./home/home.component";
import { UserRoutingModule } from "./user-routing.module";
import { LoginService } from "./login/login.service";
import { CoreModule } from "../../../app/core/core.module";
import { SharedModule } from "../../../app/shared/shared.module";
import { ContactModule } from "../contact/contact.module";
import { ProfileComponent } from "./profile/profile.component";
import { HttpClientModule } from "@angular/common/http";
import { TsInfoModule } from "../tsInfo/tsInfo.module";
@NgModule({
  declarations: [HomeComponent, ProfileComponent],
  imports: [UserRoutingModule, HttpClientModule, CoreModule.forRoot(), SharedModule.forRoot(), ContactModule,TsInfoModule],
  providers: [LoginService],
  bootstrap: []
})
export class UserModule {}
